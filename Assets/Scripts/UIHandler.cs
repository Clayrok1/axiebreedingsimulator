using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

[RequireComponent(typeof(Canvas))]
public class UIHandler : MonoBehaviour
{
    [Header("Resources refs")]
    [SerializeField]
    private Sprite m_AxieEggImage = null;

    [SerializeField]
    private Sprite m_AxieAdultImage = null;

    [Header("Other refs")]
    [SerializeField]
    private RectTransform m_RootTransform = null;

    [SerializeField]
    private TransactionIndicatorContainer m_TransactionIndicatorContainer = null;

    [SerializeReference]
    private RectTransform m_MoneyTextSection = null;

    [SerializeField]
    private TextMeshProUGUI m_MoneyText = null;

    private Canvas m_Canvas = null;


    private void Awake()
    {
        m_Canvas = GetComponent<Canvas>();
        SetMoney(0);
    }

    public void AddMoney(float _Amount)
    {
        AppHandler.Instance.Money += _Amount;
        UpdateMoneyText();
    }

    public void RemoveMoney(float _Amount)
    {
        AppHandler.Instance.Money -= _Amount;
        UpdateMoneyText();
    }

    public void SetMoney(float _Amount)
    {
        AppHandler.Instance.Money = _Amount;
        UpdateMoneyText();
    }

    public void UpdateUI()
    {
        UpdateMoneyText();
    }

    private void UpdateMoneyText()
    {
        m_MoneyText.text = "$" + AppHandler.Instance.Money.ToString("0.##", AppHandler.Instance.CultureFormatProvider);
        LayoutRebuilder.ForceRebuildLayoutImmediate(m_MoneyTextSection);
    }

    public Vector2 GetViewportSize()
    {
        Camera camera = AppHandler.Instance.CameraObject;
        Vector2 size = new Vector2(camera.pixelWidth, camera.pixelHeight);
        return size;
    }

    public Vector2 GetViewportCenterPosition()
    {
        Camera camera = AppHandler.Instance.CameraObject;
        float posX = camera.pixelWidth / 2f;
        float posY = camera.pixelHeight / 2f;
        return new Vector2(posX, posY);
    }

    public Vector2 ClampPositionToViewport(Vector2 _Position)
    {
        Camera camera = AppHandler.Instance.CameraObject;
        float posX = Mathf.Clamp(_Position.x, 0, camera.pixelWidth);
        float posY = Mathf.Clamp(_Position.y, 0, camera.pixelHeight);
        return new Vector2(posX, posY);
    }

    public Vector2 InverseTransformPoint(Vector2 _Point)
    {
        return m_Canvas.transform.InverseTransformPoint(_Point);
    }


    public Sprite AxieEggImage { get => m_AxieEggImage; }
    public Sprite AxieAdultImage { get => m_AxieAdultImage; }
    public RectTransform RootTransform { get => m_RootTransform; }
    public TransactionIndicatorContainer TransactionIndicatorContainerObject
    {
        get => m_TransactionIndicatorContainer;
    }
    public float CanvasScaleFactor { get => m_Canvas.scaleFactor; }
}