using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Shop : AxieContainerZone
{
    [SerializeField]
    private Animator m_Animator = null;

    private List<KeyValuePair<int, float>> m_NextSellsPrices = new List<KeyValuePair<int, float>>();


    private void Awake()
    {
        AppHandler.Instance.DayCounterObject.AddOnDayChangedEvent(UpdateSellPrices);
        AppHandler.Instance.RegisterResetAppEvent(ResetShop);
    }

    public void BuyAxie()
    {
        AppHandler appHandler = AppHandler.Instance;
        float buyPrice = appHandler.Settings.BuyPrice;

        if (appHandler.Spawner.TryCreateAxie(appHandler.PlaygroundObject, Axie.EAxieState.adult))
        {
            appHandler.UIHandlerObject.RemoveMoney(buyPrice);
            appHandler.UIHandlerObject.TransactionIndicatorContainerObject.ShowTransaction(-buyPrice);
        }
    }

    public void ResetShop()
    {
        m_NextSellsPrices.Clear();
    }

    public void UpdateSellPrices()
    {
        AppHandler appHandler = AppHandler.Instance;
        float totalPrice = 0;

        for (int i = m_NextSellsPrices.Count - 1; i >= 0; --i)
        {
            int currentDay = appHandler.DayCounterObject.CurrentDay;
            SettingsPanel.SettingsData settings = appHandler.Settings;

            if ((currentDay - m_NextSellsPrices[i].Key) >= settings.ShopSellDelay)
            {
                totalPrice += m_NextSellsPrices[i].Value;
                m_NextSellsPrices.RemoveAt(i);
            }
        }

        if (totalPrice > 0)
        {
            appHandler.UIHandlerObject.AddMoney(totalPrice);
            appHandler.UIHandlerObject.TransactionIndicatorContainerObject.ShowTransaction(totalPrice);
            totalPrice = 0;
        }
    }

    private void AddAxieToSellQueue(Axie _ToSell)
    {
        AppHandler appHandler = AppHandler.Instance;
        float sellPrice = appHandler.Settings.SellPrice;

        appHandler.AxieHandlerObject.DeleteAxie(_ToSell);
        m_NextSellsPrices.Add(new KeyValuePair<int, float>(appHandler.DayCounterObject.CurrentDay, sellPrice));
    }

    protected override void OnAxieAdded(Axie _Axie, bool _UseRandomPositionRange = false)
    {
        AddAxieToSellQueue(_Axie);
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);

        if (m_Animator != null && AppHandler.Instance.AxieInteractorObject.IsDraggingContent)
        {
            m_Animator.SetBool("SellReady", true);
        }
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);

        if (m_Animator != null)
        {
            m_Animator.SetBool("SellReady", false);
        }
    }
}