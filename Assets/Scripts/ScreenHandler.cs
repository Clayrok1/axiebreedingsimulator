using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenHandler : MonoBehaviour
{
#if !UNITY_ANDROID && !UNITY_IOS

    [SerializeField]
    private FullScreenMode m_FullScreenMode = FullScreenMode.MaximizedWindow;

    private Vector2Int m_SavedWindowResolution;


    private void Start()
    {
        Screen.SetResolution(1280, 720, false);
        SaveWindowSize();
    }

    public void SwitchFullscreen()
    {
        if (Screen.fullScreen)
        {
            Screen.SetResolution(m_SavedWindowResolution.x, m_SavedWindowResolution.y, false);
        }
        else
        {
            SaveWindowSize();

            Resolution resolution = Screen.currentResolution;
            Screen.SetResolution(resolution.width, resolution.height, m_FullScreenMode);
        }
    }

    private void SaveWindowSize()
    {
        m_SavedWindowResolution = new Vector2Int(Screen.width, Screen.height);
    }

#endif
}