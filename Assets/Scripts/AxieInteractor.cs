using System;
using System.Collections.Generic;
using UnityEngine;

public class AxieInteractor : MonoBehaviour
{
    [SerializeField]
    private Color m_FamilyOutlineColor = Color.green;

    private List<Axie> m_SelectedAxies = new List<Axie>();
    private List<Axie> m_FamilyHighlightedAxies = new List<Axie>();
    private bool m_IsDraggingContent = false;

    private AxieContainerZone m_BeginDragContainerZone = null;
    private Vector3 m_BeginDragPosition = Vector3.zero;


    public void ShowFamilyOf(Axie _Axie)
    {
        if (m_FamilyHighlightedAxies.Count > 0)
        {
            m_FamilyHighlightedAxies.Clear();
        }

        AxieHandler axieHandler = AppHandler.Instance.AxieHandlerObject;

        m_FamilyHighlightedAxies.AddRange(axieHandler.GetParentsOf(_Axie));
        m_FamilyHighlightedAxies.AddRange(axieHandler.GetChildrenOf(_Axie));
        m_FamilyHighlightedAxies.AddRange(axieHandler.GetSiblingsOf(_Axie));

        foreach (Axie axie in m_FamilyHighlightedAxies)
        {
            axie.ActivateOutline(m_FamilyOutlineColor);
        }
    }

    public void UnselectAxie(Axie _Axie)
    {
        if (m_SelectedAxies.Contains(_Axie))
        {
            m_SelectedAxies.Remove(_Axie);
            _Axie.OnUnselected();
        }

        foreach (Axie axie in m_FamilyHighlightedAxies)
        {
            axie.RemoveOutline();
        }
        m_FamilyHighlightedAxies.Clear();
    }

    public void UnselectAllAxies()
    {
        while (m_SelectedAxies.Count > 0)
        {
            UnselectAxie(m_SelectedAxies[0]);
        }
    }

    public void OnAxieClicked(Axie _Axie)
    {
        if (_Axie.CurrentContainer.SelectionAllowed)
        {
            if (!m_SelectedAxies.Contains(_Axie))
            {
                UnselectAllAxies();

                m_SelectedAxies.Add(_Axie);
                _Axie.OnSelected();
                ShowFamilyOf(_Axie);
            }
            else
            {
                AppHandler.Instance.TextEntryWindowObject.OpenForEdit(text => { _Axie.SetNameTagText(text); }, "Enter name:", _Axie.GetNameTagText());
            }
        }
    }

    public void OnAxieBeginDrag(Axie _Axie)
    {
        m_BeginDragContainerZone = _Axie.CurrentContainer;
        m_BeginDragPosition = _Axie.GetPosition();

        if (AxieContainerZone.TryMoveAxieTo(_Axie, AppHandler.Instance.FrontAxieContainer))
        {
            m_IsDraggingContent = true;
        }
    }

    public void OnAxieDropped(Axie _DroppedAxie)
    {
        AxieContainerZone containerZone = AxieContainerZone.HoveredContainerZone;
        if (containerZone == null || !containerZone.CanAddAxie)
        {
            AxieContainerZone.TryMoveAxieTo(_DroppedAxie, m_BeginDragContainerZone);
            _DroppedAxie.SetPosition(m_BeginDragPosition);
        }
        else
        {
            containerZone.OnAxieDropped(_DroppedAxie);
        }

        m_IsDraggingContent = false;
    }


    public bool IsDraggingContent { get => m_IsDraggingContent; }
}