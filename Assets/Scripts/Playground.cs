using UnityEngine;
using UnityEngine.EventSystems;

public class Playground : AxieContainerZone, IPointerClickHandler
{
    public override Vector3 GetAxieDefaultPosition()
    {
        return AppHandler.Instance.UIHandlerObject.GetViewportCenterPosition();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        AppHandler.Instance.AxieInteractorObject.UnselectAllAxies();
    }

    protected override void OnAxieAdded(Axie _Axie, bool _UseRandomPositionRange = false)
    {
        _Axie.ResetTransform();
        base.OnAxieAdded(_Axie, _UseRandomPositionRange);
    }
}