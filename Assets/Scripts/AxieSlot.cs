using System;
using UnityEngine;

public class AxieSlot : AxieContainerZone
{
    private Action m_StateChangedEvent = null;


    private void Awake()
    {
        m_AxieLimit = 1;
    }

    protected override void OnAxieAdded(Axie _Axie, bool _UseRandomPositionRange = false)
    {
        base.OnAxieAdded(_Axie, _UseRandomPositionRange);

        if (m_StateChangedEvent != null)
        {
            m_StateChangedEvent.Invoke();
        }
    }

    protected override void OnAxieRemoved(Axie _Axie)
    {
        base.OnAxieRemoved(_Axie);

        if (m_StateChangedEvent != null)
        {
            m_StateChangedEvent.Invoke();
        }
    }

    public void SetStateChangedEvent(Action _Event)
    {
        m_StateChangedEvent = _Event;
    }

    public Axie GetAxie()
    {
        if (m_ContainedAxies.Count == 0)
        {
            return null;
        }

        return m_ContainedAxies[0];
    }
}