using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class AppHandler : MonoBehaviour
{
    private static AppHandler m_Instance = null;

    [Header("Refs")]
    [SerializeField]
    protected AxieContainerZone m_FrontAxieContainer = null;

    [SerializeField]
    private Camera m_Camera = null;

    [SerializeField]
    private ScreenHandler m_ScreenHandler = null;

    [SerializeField]
    private UIHandler m_UIHandler = null;

    [SerializeField]
    private AxieSpawner m_Spawner = null;

    [SerializeField]
    private AxieHandler m_AxieHandler = null;

    [SerializeField]
    private AxieInteractor m_Interactor = null;

    [SerializeField]
    private Playground m_Playground = null;

    [SerializeField]
    private SettingsPanel m_SettingsPanel = null;

    [SerializeField]
    private APIRequester m_APIRequester = null;

    [SerializeField]
    private BreedPanel m_BreedPanel = null;

    [SerializeField]
    private TextEntryWindow m_TextEntryWindow = null;

    [SerializeField]
    private DayCounter m_DayCounter = null;

    private List<Action> m_ResetAppEvents = new List<Action>();

    private float m_Money = 0;


    private void Awake()
    {
        if (m_Instance != null && m_Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            m_Instance = this;
        }
    }

    public void ResetApp()
    {
        ResetMoney();

        foreach (Action resetAppEvent in m_ResetAppEvents)
        {
            resetAppEvent.Invoke();
        }

        Window.CloseAllWindows();
    }

    public void RegisterResetAppEvent(Action _Event)
    {
        m_ResetAppEvents.Add(_Event);
    }

    public void ResetMoney()
    {
        Money = 0;
        UIHandlerObject.UpdateUI();
    }

    private void Initialize()
    {
        UnityEngine.Random.InitState((int)System.DateTime.Now.Ticks);
    }

    public static AppHandler Instance 
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType<AppHandler>(true);
                m_Instance.Initialize();
            }

            return m_Instance;
        }    
    }
    public CultureInfo CultureFormatProvider { get => CultureInfo.InvariantCulture; }
    public AxieContainerZone FrontAxieContainer { get => m_FrontAxieContainer; }
    public Camera CameraObject { get => m_Camera; }
    public ScreenHandler ScreenHandlerObject { get => m_ScreenHandler; }
    public UIHandler UIHandlerObject { get => m_UIHandler; }
    public AxieSpawner Spawner { get => m_Spawner; }
    public AxieHandler AxieHandlerObject { get => m_AxieHandler; }
    public AxieInteractor AxieInteractorObject { get => m_Interactor; }
    public Playground PlaygroundObject { get => m_Playground; }
    public BreedPanel BreedPanelObject { get => m_BreedPanel; }
    public TextEntryWindow TextEntryWindowObject { get => m_TextEntryWindow; }
    public DayCounter DayCounterObject { get => m_DayCounter; }
    public APIRequester APIRequesterObject { get => m_APIRequester; }
    public SettingsPanel.SettingsData Settings { get => m_SettingsPanel.Settings; }
    public float Money 
    { 
        get => m_Money;
        set
        {
            m_Money = value;
        }
    }
}