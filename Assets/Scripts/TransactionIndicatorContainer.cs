using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransactionIndicatorContainer : MonoBehaviour
{
    [SerializeField]
    private TransactionIndicator m_TransactionIndicatorPrefab = null;

    [SerializeField]
    private Color m_PositiveTransactionColor = Color.green;

    [SerializeField]
    private Color m_NegativeTransactionColor = Color.red;

    [Header("Indicators settings")]
    [SerializeField]
    private float m_IndicatorsMoveSpeed = 0.2f;

    [SerializeField]
    private float m_IndicatorsTimeBeforeAnimation = 2.0f;

    [SerializeField]
    private GameObject m_IndicatorsAnimationTarget = null;

    private TransactionIndicator m_LastIndicator = null;


    public void ShowTransaction(float _Amount)
    {
        if (m_LastIndicator != null)
        {
            m_LastIndicator.ForceAnimation();
        }

        m_LastIndicator = Instantiate(m_TransactionIndicatorPrefab.gameObject, transform).GetComponent<TransactionIndicator>();
        m_LastIndicator.gameObject.name = "Indicator";

        Color transactionColor = Color.white;
        if (_Amount < 0)
        {
            transactionColor = m_NegativeTransactionColor;
        }
        else
        {
            transactionColor = m_PositiveTransactionColor;
        }

        m_LastIndicator.SetupTransaction(_Amount, transactionColor, m_IndicatorsMoveSpeed, m_IndicatorsTimeBeforeAnimation, m_IndicatorsAnimationTarget.transform.position);
    }
}