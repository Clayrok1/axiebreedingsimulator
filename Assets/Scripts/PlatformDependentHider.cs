using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDependentHider : MonoBehaviour
{
    [SerializeField]
    bool m_HideOnMobile = false;

    [SerializeField]
    bool m_HideOnNonMobile = false;

    [SerializeField]
    List<RuntimePlatform> m_HideOnSpecificPlatforms = new List<RuntimePlatform>();


    private void Start()
    {
        bool bIsSpecificToHide = m_HideOnSpecificPlatforms.Contains(Application.platform);
        bool bIsMobileToHide = m_HideOnMobile && Application.isMobilePlatform;
        bool bIsNonMobileToHide = m_HideOnNonMobile && !Application.isMobilePlatform;

        if (bIsSpecificToHide || bIsMobileToHide || bIsNonMobileToHide)
        {
            gameObject.SetActive(false);
        }
    }
}