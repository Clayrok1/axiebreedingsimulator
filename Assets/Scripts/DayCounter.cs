using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DayCounter : MonoBehaviour
{
    [SerializeField]
    private RectTransform m_RootTransform = null;

    [SerializeField]
    private TextMeshProUGUI m_DayCountText = null;

    private int m_CurrentDay = 1;

    private List<Action> m_OnDaychangedEvents = new List<Action>();


    private void Awake()
    {
        UpdateCounter();

        AppHandler.Instance.RegisterResetAppEvent(ResetCounter);
    }

    public void PreviousDay()
    {
        --m_CurrentDay;
        if (m_CurrentDay < 0)
        {
            m_CurrentDay = 0;
        }
        UpdateCounter();
    }

    public void NextDay()
    {
        ++m_CurrentDay;
        UpdateCounter();
    }

    public void ResetCounter()
    {
        m_CurrentDay = 1;
        UpdateCounter();
    }

    public void AddOnDayChangedEvent(Action _Event)
    {
        m_OnDaychangedEvents.Add(_Event);
    }

    private void UpdateCounter()
    {
        m_DayCountText.text = m_CurrentDay.ToString();
        LayoutRebuilder.ForceRebuildLayoutImmediate(m_RootTransform);

        foreach (Action onDayChangedEvent in m_OnDaychangedEvents)
        {
            onDayChangedEvent.Invoke();
        }

        Window.ForceUpdateAllWindows();
    }


    public int CurrentDay { get => m_CurrentDay; }
}