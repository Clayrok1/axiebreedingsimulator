using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxieHandler : MonoBehaviour
{
    private List<Axie> m_Axies = new List<Axie>();


    private void Awake()
    {
        AppHandler.Instance.DayCounterObject.AddOnDayChangedEvent(UpdateAllAxiesState);
        AppHandler.Instance.RegisterResetAppEvent(DeleteAllAxies);
    }

    public void RegisterAxie(Axie _Axie)
    {
        if (!m_Axies.Contains(_Axie))
        {
            m_Axies.Add(_Axie);
        }
    }

    public void RemoveAxie(Axie _Axie)
    {
        if (m_Axies.Contains(_Axie))
        {
            m_Axies.Remove(_Axie);
        }
    }

    public void DeleteAxie(Axie _Axie)
    {
        AppHandler.Instance.AxieInteractorObject.UnselectAxie(_Axie);
        _Axie.SetCurrentContainer(null);
        RemoveAxie(_Axie);
        Destroy(_Axie.gameObject);
    }

    public void DeleteAllAxies()
    {
        while (m_Axies.Count > 0)
        {
            DeleteAxie(m_Axies[0]);
        }

        m_Axies.Clear();
    }

    private void UpdateAllAxiesState()
    {
        foreach (Axie axie in m_Axies)
        {
            axie.UpdateAxieState();
        }
    }

    public List<Axie> GetParentsOf(Axie _Axie)
    {
        List<Axie> result = new List<Axie>();
        
        foreach (Axie axie in m_Axies)
        {
            if (_Axie.IsChildOf(axie))
            {
                result.Add(axie);
            }
        }

        return result;
    }

    public List<Axie> GetSiblingsOf(Axie _Axie)
    {
        List<Axie> result = new List<Axie>();

        foreach (Axie axie in m_Axies)
        {
            if (axie.IsSiblingOf(_Axie))
            {
                result.Add(axie);
            }
        }

        return result;
    }

    public List<Axie> GetChildrenOf(Axie _Axie)
    {
        List<Axie> result = new List<Axie>();

        foreach (Axie axie in m_Axies)
        {
            if (axie.IsChildOf(_Axie))
            {
                result.Add(axie);
            }
        }

        return result;
    }
}