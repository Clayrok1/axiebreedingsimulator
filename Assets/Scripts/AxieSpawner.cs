using UnityEngine;

public class AxieSpawner : MonoBehaviour
{
    [SerializeField]
    private Axie m_AxiePrefab = null;

    private int m_LastAvailableAxieId = 0;


    private void Awake()
    {
        AppHandler.Instance.RegisterResetAppEvent(ResetSpawner);
    }

    public void ResetSpawner()
    {
        m_LastAvailableAxieId = 0;
    }

    public Axie TryCreateAxie(AxieContainerZone _Destination, Axie.EAxieState _AxieSpawnState, Axie _Parent1 = null, Axie _Parent2 = null)
    {
        if (_Destination == null || _Destination.CanAddAxie)
        {
            Axie newAxie = Instantiate(m_AxiePrefab.gameObject, AppHandler.Instance.PlaygroundObject.ContainerTransform).GetComponent<Axie>();
            newAxie.SetAxieState(_AxieSpawnState);
            AxieContainerZone.TryMoveAxieTo(newAxie, _Destination, true);
            newAxie.gameObject.name = "Axie";

            if (_Parent1 != null && _Parent2 != null)
            {
                newAxie.SetParentsIds(_Parent1.Id, _Parent2.Id);
            }

            newAxie.SetId(m_LastAvailableAxieId);
            ++m_LastAvailableAxieId;

            return newAxie;
        }

        return null;
    }
}