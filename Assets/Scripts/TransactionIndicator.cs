using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TransactionIndicator : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI m_TransactionText = null;

    [SerializeField]
    private Animator m_Animator = null;

    private float m_MoveSpeed = 0.2f;
    private float m_TimeBeforeAnimation = 2.0f;
    private Vector2 m_StartPosition = Vector2.zero;
    private Vector2 m_TargetPosition = Vector2.zero;
    private float m_SpawnTime = 0;

    private bool m_AnimationForced = false;


    private void Awake()
    {
        m_StartPosition = transform.position;
        m_SpawnTime = Time.time;
    }

    private void Update()
    {
        if (m_AnimationForced || Time.time - m_SpawnTime > m_TimeBeforeAnimation)
        {
            transform.position = Vector2.MoveTowards(transform.position, m_TargetPosition, m_MoveSpeed * Time.deltaTime);

            float distancePercent = Vector2.Distance(m_StartPosition, transform.position) / Vector2.Distance(m_StartPosition, m_TargetPosition);
            Vector3 scale = new Vector3(1.0f - distancePercent, 1.0f - distancePercent, 1.0f);
            transform.localScale = scale;

            if (m_Animator != null)
            {
                m_Animator.SetTrigger("FadeOut");
            }

            if (distancePercent == 1)
            {
                Destroy(gameObject);
            }
        }
    }

    public void SetupTransaction(float _Amount, Color _Color, float _MoveSpeed, float _TimeBeforeAnimation, Vector2 _TargetPosition)
    {
        float unsignedAmount = Mathf.Abs(_Amount);
        string amountSign = "";
        if (_Amount < 0)
        {
            amountSign += "-";
        }
        else
        {
            amountSign += "+";
        }

        m_TransactionText.text = amountSign + "$" + unsignedAmount.ToString("0.##", AppHandler.Instance.CultureFormatProvider);
        m_TransactionText.color = _Color;
        m_MoveSpeed = _MoveSpeed;
        m_TimeBeforeAnimation = _TimeBeforeAnimation;
        m_TargetPosition = _TargetPosition;
    }

    public void ForceAnimation()
    {
        m_AnimationForced = true;
    }
}