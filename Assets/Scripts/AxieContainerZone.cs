using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AxieContainerZone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private static AxieContainerZone m_HoveredContainerZone = null;

    [SerializeField]
    protected RectTransform m_ContainerTransform = null;

    [SerializeField]
    protected float m_AxieSpawnRandomDistance = 0;

    [SerializeField]
    protected bool m_FreeMoveInside = false;

    [SerializeField]
    protected bool m_AllowNameTagsInside = false;

    [SerializeField]
    protected bool m_SelectionAllowed = true;

    [SerializeField]
    protected int m_AxieLimit = -1;
    
    protected List<Axie> m_ContainedAxies = new List<Axie>();


    public static bool TryMoveAxieTo(Axie _Axie, AxieContainerZone _ToContainerZone, bool _UseRandomPositionRange = false)
    {
        if (_ToContainerZone == null)
        {
            _Axie.SetCurrentContainer(null);
            return true;
        }
        else if (_ToContainerZone.CanAddAxie)
        {
            _ToContainerZone.m_ContainedAxies.Add(_Axie);
            _Axie.SetCurrentContainer(_ToContainerZone);
            _Axie.transform.SetParent(_ToContainerZone.ContainerTransform, true);

            _ToContainerZone.OnAxieAdded(_Axie, _UseRandomPositionRange);

            return true;
        }

        return false;
    }

    public Axie RemoveAxie(Axie _Axie)
    {
        int axieIndex = m_ContainedAxies.IndexOf(_Axie);
        if (axieIndex != -1)
        {
            Axie axie = m_ContainedAxies[axieIndex];
            m_ContainedAxies.RemoveAt(axieIndex);

            OnAxieRemoved(axie);

            return axie;
        }

        return null;
    }

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        m_HoveredContainerZone = this;
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        m_HoveredContainerZone = null;
    }

    public void OnAxieDropped(Axie _Axie)
    {
        AxieContainerZone.TryMoveAxieTo(_Axie, this);
        if (_Axie.CurrentContainer == this && !m_FreeMoveInside)
        {
            _Axie.SetPosition(GetAxieDefaultPosition());
        }
    }

    protected virtual void OnAxieAdded(Axie _Axie, bool _UseRandomPositionRange = false) 
    {
        if (!m_SelectionAllowed)
        {
            AppHandler.Instance.AxieInteractorObject.UnselectAxie(_Axie);
        }

        Vector3 axiePosition = Vector3.zero;
        if (!m_FreeMoveInside)
        {
            axiePosition = GetAxieDefaultPosition();
            _Axie.SetPosition(axiePosition);
        }
        else if(_UseRandomPositionRange)
        {
            axiePosition = _Axie.GetPosition();
            axiePosition.x += Random.Range(0.0f, AxieSpawnRandomDistance);
            axiePosition.y += Random.Range(0.0f, AxieSpawnRandomDistance);
            _Axie.SetPosition(axiePosition);
        }

        _Axie.SetNameTagActiveState(m_AllowNameTagsInside);
    }

    protected virtual void OnAxieRemoved(Axie _Axie) { }

    public virtual Vector3 GetAxieDefaultPosition()
    {
        return m_ContainerTransform.position;
    }

    public static AxieContainerZone HoveredContainerZone { get => m_HoveredContainerZone; }
    public RectTransform ContainerTransform { get => m_ContainerTransform; }
    public float AxieSpawnRandomDistance { get => m_AxieSpawnRandomDistance; }
    public int AxieCount { get => m_ContainedAxies.Count; }
    public bool IsFull
    {
        get
        {
            if (m_AxieLimit == -1)
            {
                return false;
            }

            if (AxieCount < m_AxieLimit)
            {
                return false;
            }

            return true;
        }
    }
    public bool CanAddAxie { get => !IsFull; }
    public bool SelectionAllowed { get => m_SelectionAllowed; }
}