using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextEntryWindow : Window
{
    [SerializeField]
    private TextMeshProUGUI m_Label = null;

    [SerializeField]
    private TMP_InputField m_InputField = null;

    [SerializeField]
    private Button m_ConfirmButton = null;

    private Action<string> m_EditCallback = null;


    private void Update()
    {
        if ((Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) && m_ConfirmButton != null)
        {
            m_ConfirmButton.onClick.Invoke();
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            Close();
        }
    }

    public void OpenForEdit(Action<string> _EditCallback, string _Title, string _DefaultText = "")
    {
        m_EditCallback = _EditCallback;
        m_Label.text = _Title;
        m_InputField.text = _DefaultText;
        m_RootTransform.gameObject.SetActive(true);
        m_RootTransform.SetAsLastSibling();

        m_InputField.Select();
        m_InputField.ActivateInputField();
    }

    public void Confirm()
    {
        if (m_EditCallback != null)
        {
            m_EditCallback.Invoke(m_InputField.text);
        }

        Close();
    }

    public override void Close()
    {
        m_InputField.text = "";
        base.Close();
    }
}