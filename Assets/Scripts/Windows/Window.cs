using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Window : MonoBehaviour, IPointerDownHandler
{
    private static List<Window> m_OpenedWindows = new List<Window>();

    [SerializeField]
    protected RectTransform m_RootTransform = null;

    protected bool m_IsOpened = false;


    public static void CloseAllWindows()
    {
        while (m_OpenedWindows.Count > 0)
        {
            m_OpenedWindows[0].Close();
        }

        m_OpenedWindows.Clear();
    }

    public static void ForceUpdateAllWindows(Window _Exclude = null)
    {
        foreach (Window window in m_OpenedWindows)
        {
            if (window != _Exclude)
            {
                window.ForceUpdate();
            }
        }
    }

    private void Awake()
    {
        m_OpenedWindows.Add(this);
    }

    public Vector2 GetWindowSize(bool _Scaled = false)
    {
        Vector2 size = (transform as RectTransform).sizeDelta;
        return _Scaled == false ? size : size * AppHandler.Instance.UIHandlerObject.CanvasScaleFactor;
    }

    public virtual void Open() 
    {
        m_RootTransform.gameObject.SetActive(true);
        m_RootTransform.SetAsLastSibling();
        m_IsOpened = true;
    }

    public virtual void Close()
    {
        m_OpenedWindows.Remove(this);
        m_IsOpened = false;
        m_RootTransform.gameObject.SetActive(false);
    }

    public virtual void ForceUpdate() { }

    public void OnPointerDown(PointerEventData eventData)
    {
        m_RootTransform.SetAsLastSibling();
    }


    public bool IsOpened { get => m_IsOpened; }
}