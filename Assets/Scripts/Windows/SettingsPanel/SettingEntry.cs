using UnityEngine;
using TMPro;

public class SettingEntry : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField]
    private TextMeshProUGUI m_Label = null;

    [SerializeField]
    private TMP_InputField m_InputField = null;

    [SerializeField]
    private TextMeshProUGUI m_PlaceHolder = null;

    [Header("Settings")]
    [SerializeField]
    private string m_PlaceHolderText = "";

    [SerializeField]
    private string m_AllowedCharacters = "";

    private Color m_LabelStartColor = Color.white;



    private void Start()
    {
        m_PlaceHolder.text = m_PlaceHolderText;
        m_LabelStartColor = m_Label.color;
    }

    public bool ValidateEntry()
    {
        if (IsEntryValid())
        {
            m_Label.color = m_LabelStartColor;
            return true;
        }


        m_Label.color = Color.red;
        return false;
    }

    public bool IsEntryValid()
    {
        return IsStringAllowed(m_InputField.text);
    }

    public string GetText()
    {
        return m_InputField.text;
    }

    public void SetText(string _Text)
    {
        if (IsStringAllowed(_Text))
        {
            m_InputField.text = _Text;
        }
    }


    private bool IsStringAllowed(string _String)
    {
        if (m_AllowedCharacters != "")
        {
            foreach (char character in _String)
            {
                if (!m_AllowedCharacters.Contains(character.ToString()))
                {
                    return false;
                }
            }
        }

        return true;
    }
}
