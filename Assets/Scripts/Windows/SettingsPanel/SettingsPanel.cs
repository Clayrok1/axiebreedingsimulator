using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPanel : Window
{
    [System.Serializable]
    public class SettingsData
    {
        [SerializeField]
        private List<int> BreedCosts = new List<int>();
        public int AXSBreedQuantity = 2;
        public float AXSPrice = 75;
        public float SLPPrice = 0.13f;
        public float BuyPrice = 300;
        public float SellPrice = 400;
        public int HatchingDelay = 5;
        public int ShopSellDelay = 5;

        public int GetBreedCost(int _CurrentBreedCount)
        {
            if (_CurrentBreedCount < BreedCosts.Count - 1)
            {
                return BreedCosts[_CurrentBreedCount];
            }

            return -1;
        }
    }

    [SerializeField]
    private SettingsData m_Settings;

    [Header("Settings Entries Refs")]
    [SerializeField]
    private SettingEntry m_AXSBreedQuantitySetting = null;

    [SerializeField]
    private SettingEntry m_AXSPriceSetting = null;

    [SerializeField]
    private SettingEntry m_SLPPriceSetting = null;

    [SerializeField]
    private SettingEntry m_BuyPriceSetting = null;

    [SerializeField]
    private SettingEntry m_SellPriceSetting = null;

    [SerializeField]
    private SettingEntry m_HatchingDelaySetting = null;

    [SerializeField]
    private SettingEntry m_ShopSellDelaySetting = null;

    [Header("Other refs")]
    [SerializeField]
    private Animator m_RefreshTokenValuesButtonAnimator = null;

    private bool m_IsFetchingTokenPrices = false;


    private void Start()
    {
        m_AXSBreedQuantitySetting.SetText(m_Settings.AXSBreedQuantity.ToString(AppHandler.Instance.CultureFormatProvider));
        m_AXSPriceSetting.SetText(m_Settings.AXSPrice.ToString(AppHandler.Instance.CultureFormatProvider));
        m_SLPPriceSetting.SetText(m_Settings.SLPPrice.ToString(AppHandler.Instance.CultureFormatProvider));
        m_BuyPriceSetting.SetText(m_Settings.BuyPrice.ToString(AppHandler.Instance.CultureFormatProvider));
        m_SellPriceSetting.SetText(m_Settings.SellPrice.ToString(AppHandler.Instance.CultureFormatProvider));
        m_HatchingDelaySetting.SetText(m_Settings.HatchingDelay.ToString());
        m_ShopSellDelaySetting.SetText(m_Settings.ShopSellDelay.ToString());

        LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
    }

    public override void ForceUpdate()
    {
        m_Settings.AXSBreedQuantity = int.Parse(m_AXSBreedQuantitySetting.GetText(), AppHandler.Instance.CultureFormatProvider);
        m_Settings.AXSPrice = float.Parse(m_AXSPriceSetting.GetText(), AppHandler.Instance.CultureFormatProvider);
        m_Settings.SLPPrice = float.Parse(m_SLPPriceSetting.GetText(), AppHandler.Instance.CultureFormatProvider);
        m_Settings.BuyPrice = float.Parse(m_BuyPriceSetting.GetText(), AppHandler.Instance.CultureFormatProvider);
        m_Settings.SellPrice = float.Parse(m_SellPriceSetting.GetText(), AppHandler.Instance.CultureFormatProvider);
        m_Settings.HatchingDelay = int.Parse(m_HatchingDelaySetting.GetText());
        m_Settings.ShopSellDelay = int.Parse(m_ShopSellDelaySetting.GetText());
    }

    public override void Close()
    {
        if (!m_IsFetchingTokenPrices)
        {
            bool areEntriesValid = true;

            List<SettingEntry> settingsToValidate = new List<SettingEntry>();

            settingsToValidate.Add(m_AXSBreedQuantitySetting);
            settingsToValidate.Add(m_AXSPriceSetting);
            settingsToValidate.Add(m_SLPPriceSetting);
            settingsToValidate.Add(m_BuyPriceSetting);
            settingsToValidate.Add(m_SellPriceSetting);
            settingsToValidate.Add(m_HatchingDelaySetting);
            settingsToValidate.Add(m_ShopSellDelaySetting);

            foreach (SettingEntry settingEntry in settingsToValidate)
            {
                if (!settingEntry.ValidateEntry())
                {
                    areEntriesValid = false;
                }
            }

            if (areEntriesValid)
            {
                ForceUpdate();
                Window.ForceUpdateAllWindows(this);
                base.Close();
            }
        }
    }

    public void OnRefreshTokenPricesClicked()
    {
        m_IsFetchingTokenPrices = true;

        if (m_RefreshTokenValuesButtonAnimator != null)
        {
            m_RefreshTokenValuesButtonAnimator.SetBool("IsFetching", true);
        }
        
        AppHandler.Instance.APIRequesterObject.StartRequestGetTokensPricesToUSD(RefreshTokenPricesValues, () => {
            SetRefreshTokenValuesButtonFetchingAnimationState(false);
        }, "smooth-love-potion,axie-infinity");
    }

    private void RefreshTokenPricesValues(Dictionary<string, float> tokenPrices)
    {
        SetRefreshTokenValuesButtonFetchingAnimationState(false);

        AppHandler appHandler = AppHandler.Instance;
        m_SLPPriceSetting.SetText(tokenPrices["smooth-love-potion"].ToString("0.###", appHandler.CultureFormatProvider));
        m_AXSPriceSetting.SetText(tokenPrices["axie-infinity"].ToString("0.##", appHandler.CultureFormatProvider));

        m_IsFetchingTokenPrices = false;
    }

    private void SetRefreshTokenValuesButtonFetchingAnimationState(bool _State)
    {
        if (m_RefreshTokenValuesButtonAnimator != null)
        {
            m_RefreshTokenValuesButtonAnimator.SetBool("IsFetching", _State);
        }
    }

    public SettingsData Settings { get => m_Settings; }
}