using UnityEngine;
using UnityEngine.EventSystems;

public class MoveWindowHandle : MonoBehaviour, IBeginDragHandler, IDragHandler
{
    [SerializeField]
    private Window m_WindowToMove = null;

    private Vector3 m_StartDragMouseOffset = Vector3.zero;


    public void OnBeginDrag(PointerEventData eventData)
    {
        m_StartDragMouseOffset = Input.mousePosition - m_WindowToMove.transform.position;
        m_WindowToMove.transform.SetAsLastSibling();
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 viewportSize = AppHandler.Instance.UIHandlerObject.GetViewportSize();
        Vector2 windowSize = m_WindowToMove.GetWindowSize(true);

        Vector3 position = Input.mousePosition - m_StartDragMouseOffset;
        position.x = Mathf.Clamp(position.x, windowSize.x / 2f, viewportSize.x - windowSize.x / 2f);
        position.y = Mathf.Clamp(position.y, windowSize.y, viewportSize.y);

        m_WindowToMove.transform.position = position;
    }
}