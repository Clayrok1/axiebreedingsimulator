using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BreedPanel : Window
{
    [Header("Breed Button")]
    [SerializeField]
    private Button m_BreedButton = null;

    [SerializeField]
    private TextMeshProUGUI m_BreedButtonText = null;

    [SerializeField]
    private Color m_BreedButtonTextActiveColor = Color.white;

    [SerializeField]
    private Color m_BreedButtonTextDisabledColor = Color.grey;

    [Header("Parent 1")]
    [SerializeField]
    private AxieSlot m_ParentSlot1 = null;

    [SerializeField]
    private TextMeshProUGUI m_BreedCountParent1 = null;

    [SerializeField]
    private GameObject m_BreedSLPQuantitySectionParent1 = null;

    [SerializeField]
    private TextMeshProUGUI m_BreedCostTextParent1 = null;

    [Header("Parent 2")]
    [SerializeField]
    private AxieSlot m_ParentSlot2 = null;

    [SerializeField]
    private TextMeshProUGUI m_BreedCountParent2 = null;

    [SerializeField]
    private GameObject m_BreedSLPQuantitySectionParent2 = null;

    [SerializeField]
    private TextMeshProUGUI m_BreedCostTextParent2 = null;

    [Header("Child")]
    [SerializeField]
    private AxieSlot m_ChildSlot = null;

    [Header("Misc")]
    [SerializeField]
    private RectTransform m_BreedQuantityCostTransform = null;

    [SerializeField]
    private TextMeshProUGUI m_SLPBreedQuantityText = null;

    [SerializeField]
    private TextMeshProUGUI m_AXSBreedQuantityText = null;

    [SerializeField]
    private TextMeshProUGUI m_DollarBreedCostText = null;

    [SerializeField]
    private List<Image> m_BreedLinesElements = new List<Image>();

    [SerializeField]
    private Color m_BreedLinesElementsEnabledColor = Color.grey;

    [SerializeField]
    private Color m_BreedLinesElementsDisabledColor = Color.grey;

    private int m_MaxBreedCount = 5;
    private int m_Parent1BreedSLPQuantity = 0;
    private int m_Parent2BreedSLPQuantity = 0;


    private void Awake()
    {
        m_ParentSlot1.SetStateChangedEvent(ForceUpdate);
        m_ParentSlot2.SetStateChangedEvent(ForceUpdate);
        m_ChildSlot.SetStateChangedEvent(ForceUpdate);

        ForceUpdate();

        AppHandler.Instance.DayCounterObject.AddOnDayChangedEvent(ForceUpdate);
    }

    public void Breed()
    {
        if (!m_ChildSlot.IsFull)
        {
            Axie parent1 = m_ParentSlot1.GetAxie();
            Axie parent2 = m_ParentSlot2.GetAxie();

            if (AppHandler.Instance.Spawner.TryCreateAxie(m_ChildSlot, Axie.EAxieState.egg, parent1, parent2) != null)
            {
                AppHandler appHandler = AppHandler.Instance;
                UIHandler ui = appHandler.UIHandlerObject;
                SettingsPanel.SettingsData settings = appHandler.Settings;
                float parent1BreedCost = settings.GetBreedCost(parent1.BreedCount);
                float parent2BreedCost = settings.GetBreedCost(parent2.BreedCount);
                float totalBreedCost = settings.SLPPrice * parent1BreedCost + settings.SLPPrice * parent2BreedCost + settings.AXSBreedQuantity * settings.AXSPrice;

                parent1.BreedCount++;
                parent2.BreedCount++;

                ui.RemoveMoney(totalBreedCost);
                ui.TransactionIndicatorContainerObject.ShowTransaction(-totalBreedCost);

                ForceUpdate();
            }
        }
    }

    public override void Close()
    {
        if (!m_ParentSlot1.IsFull && !m_ParentSlot2.IsFull && !m_ChildSlot.IsFull)
        {
            base.Close();
        }
    }

    public override void ForceUpdate()
    {
        Axie parent1 = m_ParentSlot1.GetAxie();
        Axie parent2 = m_ParentSlot2.GetAxie();

        bool canParent1Breed = false;
        bool canParent2Breed = false;

        if (parent1 != null && parent2 != null)
        {
            bool bBreedLeftP1 = m_ParentSlot1.GetAxie().BreedCount < m_MaxBreedCount;
            bool bBreedLeftP2 = m_ParentSlot2.GetAxie().BreedCount < m_MaxBreedCount;

            bool bIsAdultP1 = parent1.CurrentAxieState != Axie.EAxieState.egg;
            bool bIsAdultP2 = parent2.CurrentAxieState != Axie.EAxieState.egg;

            bool bAxiesFromSameFamily = parent1.IsChildOf(parent2) || parent1.IsSiblingOf(parent2) || parent2.IsChildOf(parent1) || parent2.IsSiblingOf(parent1);

            canParent1Breed = bIsAdultP1 && bBreedLeftP1 && !bAxiesFromSameFamily;
            canParent2Breed = bIsAdultP2 && bBreedLeftP2 && !bAxiesFromSameFamily;
        }

        SetBreedElementsActiveState(canParent1Breed && canParent2Breed && !m_ChildSlot.IsFull);
        UpdateParentSlot1(canParent1Breed);
        UpdateParentSlot2(canParent2Breed);
        UpdateBreedInfos();

        LayoutRebuilder.ForceRebuildLayoutImmediate(m_BreedQuantityCostTransform);
    }

    private void UpdateParentSlot1(bool _CanBreed)
    {
        if (!m_ParentSlot1.IsFull)
        {
            m_BreedCountParent1.text = "Parent 1";
            m_BreedSLPQuantitySectionParent1.SetActive(false);
            m_Parent1BreedSLPQuantity = 0;
        }
        else
        {
            int axieBreedCount = m_ParentSlot1.GetAxie().BreedCount;
            m_BreedSLPQuantitySectionParent1.SetActive(true);
            m_BreedCountParent1.text = axieBreedCount.ToString() + "/" + m_MaxBreedCount.ToString();

            m_Parent1BreedSLPQuantity = AppHandler.Instance.Settings.GetBreedCost(axieBreedCount);

            if (axieBreedCount < m_MaxBreedCount)
            {
                m_BreedCostTextParent1.text = m_Parent1BreedSLPQuantity.ToString("0.##", AppHandler.Instance.CultureFormatProvider);
            }
            else
            {
                m_BreedCostTextParent1.text = "MAX";
            }
        }
    }

    private void UpdateParentSlot2(bool _CanBreed)
    {
        if (!m_ParentSlot2.IsFull)
        {
            m_BreedCountParent2.text = "Parent 2";
            m_BreedSLPQuantitySectionParent2.SetActive(false);
            m_Parent2BreedSLPQuantity = 0;
        }
        else
        {
            int axieBreedCount = m_ParentSlot2.GetAxie().BreedCount;
            m_BreedSLPQuantitySectionParent2.SetActive(true);
            m_BreedCountParent2.text = axieBreedCount.ToString() + "/" + m_MaxBreedCount.ToString();

            m_Parent2BreedSLPQuantity = AppHandler.Instance.Settings.GetBreedCost(axieBreedCount);

            if (axieBreedCount < m_MaxBreedCount)
            {
                m_BreedCostTextParent2.text = m_Parent2BreedSLPQuantity.ToString();
            }
            else
            {
                m_BreedCostTextParent2.text = "MAX";
            }
        }
    }

    private void UpdateBreedInfos()
    {
        Axie parent1 = m_ParentSlot1.GetAxie();
        Axie parent2 = m_ParentSlot2.GetAxie();

        int TotalSLPBreedQuantity = 0;
        if (!parent1 && !parent2 || parent1 && parent1.BreedCount >= m_MaxBreedCount || parent2 && parent2.BreedCount >= m_MaxBreedCount)
        {
            m_SLPBreedQuantityText.text = "_";
        }
        else
        {
            TotalSLPBreedQuantity = m_Parent1BreedSLPQuantity + m_Parent2BreedSLPQuantity;
            m_SLPBreedQuantityText.text = TotalSLPBreedQuantity.ToString();
        }

        SettingsPanel.SettingsData settings = AppHandler.Instance.Settings;
        int axsQuantity = settings.AXSBreedQuantity;
        m_AXSBreedQuantityText.text = axsQuantity.ToString();

        float totalCost = axsQuantity * settings.AXSPrice + TotalSLPBreedQuantity * settings.SLPPrice;
        m_DollarBreedCostText.text = totalCost.ToString("0.##", AppHandler.Instance.CultureFormatProvider);
    }

    private void SetBreedElementsActiveState(bool _State)
    {
        Color breedLinesColor = Color.white;

        if (_State)
        {
            m_BreedButton.interactable = true;
            m_BreedButtonText.color = m_BreedButtonTextActiveColor;
            breedLinesColor = m_BreedLinesElementsEnabledColor;
        }
        else
        {
            m_BreedButton.interactable = false;
            m_BreedButtonText.color = m_BreedButtonTextDisabledColor;
            breedLinesColor = m_BreedLinesElementsDisabledColor;
        }

        foreach (Image line in m_BreedLinesElements)
        {
            line.color = breedLinesColor;
        }
    }
}