using UnityEngine;
using UnityEngine.EventSystems;

public class WindowSwitcher : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private Window m_Window = null;


    public void OnPointerClick(PointerEventData eventData)
    {
        if (m_Window.IsOpened)
        {
            m_Window.Close();
        }
        else
        {
            m_Window.Open();
        }
    }
}
