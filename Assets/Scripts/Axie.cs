using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

[RequireComponent(typeof(Image))]
public class Axie : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler
{
    public enum EAxieState
    {
        egg,
        adult
    }

    [SerializeField]
    private Image m_AxieGraphics = null;

    [Header("Outline")]
    [SerializeField]
    private RectTransform m_AxieOutlineRoot = null;

    [SerializeField]
    private Image m_AxieOutlineImage = null;

    [SerializeField]
    private Outline m_AxieOutlineComponent = null;

    [Header("Name Tag")]
    [SerializeField]
    private RectTransform m_NameTagRoot = null;

    [SerializeField]
    private TextMeshProUGUI m_NameTagText = null;

    private Image m_RootImageComponent = null;
    private AxieContainerZone m_CurrentContainer = null;

    private int m_Id = -1;
    private int m_Parent1Id = -1;
    private int m_Parent2Id = -1;
    private int m_BreedCount = 0;

    private Vector3 m_StartSizeDelta = Vector2.zero;
    private Vector3 m_StartScale = Vector3.one;

    private Vector3 m_MouseOffset = Vector3.zero;
    private bool m_IsDragging = false;

    private EAxieState m_CurrentAxieState = EAxieState.adult;
    private int m_EggCreationDay = -1;


    private void Awake()
    {
        m_StartSizeDelta = (transform as RectTransform).sizeDelta;
        m_StartScale = transform.localScale;

        m_RootImageComponent = GetComponent<Image>();

        m_AxieGraphics.sprite = m_RootImageComponent.sprite;
        m_AxieOutlineImage.sprite = m_RootImageComponent.sprite;
        m_RootImageComponent.sprite = null;

        AppHandler.Instance.AxieHandlerObject.RegisterAxie(this);
    }

    public void UpdateAxieState()
    {
        if (m_EggCreationDay != -1)
        {
            AppHandler appHandler = AppHandler.Instance;
            int currentDay = appHandler.DayCounterObject.CurrentDay;
            int daysBeforeHatching = appHandler.Settings.HatchingDelay;

            if (currentDay < m_EggCreationDay + daysBeforeHatching)
            {
                if (m_CurrentAxieState != EAxieState.egg)
                {
                    SetAxieState(EAxieState.egg);
                }
            }
            else
            {
                if (m_CurrentAxieState != EAxieState.adult)
                {
                    SetAxieState(EAxieState.adult);
                }
            }
        }
    }

    public void ResetTransform()
    {
        (transform as RectTransform).sizeDelta = m_StartSizeDelta;
        transform.localScale = m_StartScale;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        m_RootImageComponent.raycastTarget = false;

        AppHandler.Instance.AxieInteractorObject.OnAxieBeginDrag(this);

        m_MouseOffset = -(Input.mousePosition - transform.position);
        transform.SetAsLastSibling();
        m_IsDragging = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = AppHandler.Instance.UIHandlerObject.ClampPositionToViewport(m_MouseOffset + Input.mousePosition);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        m_RootImageComponent.raycastTarget = true;

        AppHandler.Instance.AxieInteractorObject.OnAxieDropped(this);

        m_IsDragging = false;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (m_IsDragging)
        {
            m_IsDragging = false;
            return;
        }

        AppHandler.Instance.AxieInteractorObject.OnAxieClicked(this);
    }

    public void OnSelected()
    {
        ActivateOutline(Color.white);
    }

    public void OnUnselected()
    {
        RemoveOutline();
    }

    public void ActivateOutline(Color _Color)
    {
        if (!m_AxieOutlineRoot.gameObject.activeInHierarchy)
        {
            if (m_AxieOutlineImage.sprite != m_AxieGraphics.sprite)
            {
                m_AxieOutlineImage.sprite = m_AxieGraphics.sprite;
            }

            m_AxieOutlineImage.color = _Color;
            m_AxieOutlineComponent.effectColor = _Color;
            m_AxieOutlineRoot.gameObject.SetActive(true);
        }
    }

    public void RemoveOutline()
    {
        m_AxieOutlineRoot.gameObject.SetActive(false);
    }

    public bool IsChildOf(Axie _Axie)
    {
        return m_Parent1Id == _Axie.Id || m_Parent2Id == _Axie.Id;
    }

    public bool IsSiblingOf(Axie _Axie)
    {
        return m_Parent1Id != -1 && m_Parent2Id != -1 && m_Parent1Id == _Axie.Parent1Id && m_Parent2Id == _Axie.Parent2Id;
    }

    public void SetAxieState(EAxieState _AxieState)
    {
        if (m_CurrentAxieState != _AxieState)
        {
            m_CurrentAxieState = _AxieState;
        }

        AppHandler appHandler = AppHandler.Instance;
        UIHandler uiHandler = appHandler.UIHandlerObject;
        switch (_AxieState)
        {
            case EAxieState.egg:
                m_AxieGraphics.sprite = uiHandler.AxieEggImage;
                if (m_EggCreationDay == -1)
                {
                    m_EggCreationDay = appHandler.DayCounterObject.CurrentDay;
                }
                break;
            case EAxieState.adult:
                m_AxieGraphics.sprite = uiHandler.AxieAdultImage;
                break;
        }
    }

    public void SetPosition(Vector2 _Position)
    {
        (transform as RectTransform).position = _Position;
    }

    public Vector2 GetPosition()
    {
        return (transform as RectTransform).position;
    }

    public void SetBreedCount(int _Count)
    {
        m_BreedCount = _Count;
    }

    public void SetNameTagActiveState(bool _Active)
    {
        if (m_NameTagRoot.gameObject.activeInHierarchy != _Active)
        {
            if (_Active)
            {
                m_NameTagRoot.gameObject.SetActive(m_NameTagText.text.Length > 0);
            }
            else
            {
                m_NameTagRoot.gameObject.SetActive(false);
            }
        }
    }

    public string GetNameTagText()
    {
        return m_NameTagText.text;
    }

    public void SetNameTagText(string _Text)
    {
        if (m_NameTagText.text != _Text)
        {
            m_NameTagText.text = _Text;

            if (_Text != "")
            {
                SetNameTagActiveState(true);
                LayoutRebuilder.ForceRebuildLayoutImmediate(m_NameTagRoot);
            }
            else
            {
                SetNameTagActiveState(false);
            }
        }
    }

    public void SetCurrentContainer(AxieContainerZone _Container)
    {
        if (m_CurrentContainer != _Container)
        {
            if (m_CurrentContainer != null)
            {
                m_CurrentContainer.RemoveAxie(this);
            }

            m_CurrentContainer = _Container;
        }
    }

    public void SetParentsIds(int _Parent1Id, int _Parent2Id)
    {
        m_Parent1Id = _Parent1Id;
        m_Parent2Id = _Parent2Id;
    }

    public void SetId(int _Id)
    {
        m_Id = _Id;
    }


    public int Id { get => m_Id; }
    public int Parent1Id { get => m_Parent1Id; }
    public int Parent2Id { get => m_Parent2Id; }
    public int BreedCount 
    { 
        get => m_BreedCount;
        set
        {
            m_BreedCount = value;
        }
    }
    public EAxieState CurrentAxieState { get => m_CurrentAxieState; }
    public AxieContainerZone CurrentContainer
    {
        get => m_CurrentContainer;
        set
        {
            m_CurrentContainer = value;
        }
    }
}