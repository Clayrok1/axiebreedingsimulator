using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class APIRequester : MonoBehaviour
{
    [SerializeField]
    private float m_TimeBetweenRequests = 10000.0f;

    [SerializeField]
    private float m_TimeoutDelay = 5000.0f;

    public static bool m_RequestCancelled = false;
    private static Action m_OnRequestFail = null;

    private IEnumerator<UnityWebRequestAsyncOperation> m_GetTokensPricesToUSDCoroutine = null;
    private IEnumerator m_CancelWhenTimeoutCoroutine = null;
    private string m_LastRequestJsonResult = "";
    private float m_LastRequestTime = -1;


    public void StartRequestGetTokensPricesToUSD(Action<Dictionary<string, float>> _OnSuccess, Action _OnFail, string _Ids)
    {
        m_GetTokensPricesToUSDCoroutine = GetTokensPricesToUSD(_OnSuccess, _OnFail, _Ids);
        m_CancelWhenTimeoutCoroutine = CancelCoroutineWhenTimeout();

        StartCoroutine(m_GetTokensPricesToUSDCoroutine);
        StartCoroutine(m_CancelWhenTimeoutCoroutine);
    }

    public void CancelCancelCoroutineWhenTimeout()
    {
        if (m_CancelWhenTimeoutCoroutine != null)
        {
            StopCoroutine(m_CancelWhenTimeoutCoroutine);
        }
    }

    private IEnumerator CancelCoroutineWhenTimeout()
    {
        float callTime = Time.time;

        while (Time.time - callTime < m_TimeoutDelay / 1000f)
        {
            yield return null;
        }

        m_RequestCancelled = true;
        m_LastRequestTime = -1;
    }

    private IEnumerator<UnityWebRequestAsyncOperation> GetTokensPricesToUSD(Action<Dictionary<string, float>> _OnSuccess, Action _OnFail, string _Ids)
    {
        m_RequestCancelled = false;
        m_OnRequestFail = _OnFail;

        _Ids = _Ids.Replace(" ", "");
        string json = "{}";

        if (m_LastRequestTime != -1 && Time.time - m_LastRequestTime < m_TimeBetweenRequests / 1000.0f)
        {
            json = m_LastRequestJsonResult;
        }
        else
        {
            using (UnityWebRequest req = UnityWebRequest.Get(string.Format("https://api.coingecko.com/api/v3/simple/price?ids={0}&vs_currencies=usd", _Ids)))
            {
                yield return req.SendWebRequest();
                while (!req.isDone)
                {
                    yield return null;
                }

                if (!m_RequestCancelled)
                {
                    byte[] result = req.downloadHandler.data;
                    json = System.Text.Encoding.Default.GetString(result);
                    m_LastRequestJsonResult = json;

                    m_LastRequestTime = json == "{}" ? m_LastRequestTime : Time.time;
                }
            }
        }

        m_GetTokensPricesToUSDCoroutine = null;

        if (!m_RequestCancelled)
        {
            CancelCancelCoroutineWhenTimeout();
            _OnSuccess.Invoke(ParseTokensPriceResult(json));
        }
        else if (_OnFail != null)
        {
            _OnFail.Invoke();
        }
    }

    private Dictionary<string, float> ParseTokensPriceResult(string _JsonResult)
    {
        Dictionary<string, float> results = null;

        if (_JsonResult != "{}")
        {
            results = new Dictionary<string, float>();

            _JsonResult = _JsonResult.Replace("{", "").Replace("}", "").Replace("\"", "").Replace("usd:", "");
            string[] unparsedResults = _JsonResult.Split(',');

            foreach (string unparsedResult in unparsedResults)
            {
                string[] parsedResult = unparsedResult.Split(':');
                results.Add(parsedResult[0], float.Parse(parsedResult[1], AppHandler.Instance.CultureFormatProvider));
            }
        }

        return results;
    }


    public string LastJsonResult { get => m_LastRequestJsonResult; }
}